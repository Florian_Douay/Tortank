package thinktank.javabot.graphics;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import thinktank.javabot.physics.Coordinates;
import thinktank.javabot.physics.Game;
import thinktank.javabot.physics.Tools;


@SuppressWarnings("serial")
public class MainWindow extends JFrame {
	
	private static DrawPanel grid;
	private Game game;
	
	private static Editor editor = new Editor();
	private static Menu m_menu = new Menu();
	
	private static JButton start = new JButton("Start");
	private static JButton raz	  = new JButton("Reset");
	
	private JPanel container = new JPanel();
	private JPanel south_container = new JPanel();

	
	/**
	 * Affichage principal de l'application
	 **/
	public MainWindow() {
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		
		container.setPreferredSize(new Dimension((int)width,(int)height));
		
		int squareNumberOnX = 42;
		int squareNumberOnY = 24;
		game = new Game(squareNumberOnX, squareNumberOnY);
		grid = new DrawPanel(game);
		
		this.installListeners();
		//this.installTooltips();
		
		game.setView(grid);
		
		this.setTitle("Tortank");
		this.setSize((int)1267,(int)851);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
        
        container.setLayout(new GridBagLayout());
	
        /* position et taille des �l�ments */
		GridBagConstraints gc = new GridBagConstraints();
		
		// new Insets(margeSup�rieure, margeGauche, margeInf�rieur, margeDroite) */
		gc.insets = new Insets(7,7,7,7);
		
		// place du composant s'il n'occupe pas la totalit� de l'espace disponible */
		gc.ipady = gc.anchor = GridBagConstraints.CENTER;
		
		// ajout composant en position (i, j)
		gc.gridx = 0;
		gc.gridy = 1;
		
		gc.fill = GridBagConstraints.BOTH; /* rempli l'espace en vertical et horizontal si possible */
		gc.weightx = 1;
		gc.weighty = 1;
		
		gc.gridheight = 2;
			
		// ajoute le composant au panel en pr�cisant le GridBagConstraints */
		container.add(m_menu.getContainer(), gc);
		
		gc.gridx = 1;
		gc.gridy = 0;
		
		gc.fill = GridBagConstraints.BOTH;
		gc.weightx = 4;
		gc.weighty = 10;
		gc.gridwidth = 3;
		container.add(grid, gc);
		container.add(editor.getContainer(), gc);
		hideEditorPanel();
		
		gc.gridx = 1;
		gc.gridy = 2;
		gc.fill = GridBagConstraints.BOTH;	
		gc.gridwidth = 3;
		gc.gridheight = 1;
		gc.weightx = 1;
		gc.weighty = 1;
		container.add(south_container, gc);
		
		this.installSouthContainer();
		this.installTooltips();
		
		this.setContentPane(container);
		this.setResizable(false);
		this.setVisible(true);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		grid.repaint();
	
	}
	
	/**Graphics
	 * Fonction main principale
	 **/
	@SuppressWarnings("unused")
	public static void main(String args[]) {
		
		final MainWindow window = new MainWindow();
	}
	
	public void installSouthContainer(){
		int size_x = 700,
			size_y = 70;
		start.setPreferredSize(new Dimension(size_x,size_y));
		raz.setPreferredSize(new Dimension(size_x, size_y));
		 
		south_container.setLayout(new GridBagLayout());
		 
		GridBagConstraints gbc = new GridBagConstraints();
		 
		gbc.gridx = 0;
		gbc.gridy = GridBagConstraints.RELATIVE;
		 
		gbc.fill = GridBagConstraints.VERTICAL;
		 
		gbc.insets = new Insets(0, 0, 0, 0);
		 
		 
		south_container.add(start, gbc);
		south_container.add(raz, gbc);
	}
	
	public void installListeners(){
		// listener des boutons du bas	
		start.addActionListener(new ActionListener(){
	         
		    @Override
		    public void actionPerformed(ActionEvent e){
		    	m_menu.setEnabledButton(false);
		    	if(game.IsLaunched())
		    	{
		    		game.stopGame();
		    		start.setText("Resume");
		    		m_menu.setEnabled(true);
		    		m_menu.setEnabledButton(false);
		    	}
		    	else
		    	{
		    		game.startGame();
		    		start.setText("Pause");
		    		m_menu.setEnabled(false);
		    	}
		    }
		});
		
		raz.addActionListener(new ActionListener(){
		    @Override
		    public void actionPerformed(ActionEvent e){
		    	m_menu.setEnabledButton(true);
		    	game.stopGame();
		    	game.clearBattleField();
		    	start.setText("Start");
		    	m_menu.setEnabled(true);
		    }
		});
		
		
		// listener de la grid
		grid.addMouseListener(new MouseListener(){
			@Override
			public void mouseClicked(MouseEvent e){
				if(e.getClickCount()==2){
					onGridDoubleClicked(e.getX()/24, e.getY()/24);
				}
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {}

			@Override
			public void mouseExited(MouseEvent arg0) {}

			@Override
			public void mousePressed(MouseEvent arg0) {}

			@Override
			public void mouseReleased(MouseEvent arg0) {}
		});
		
		
		// listener du container global
		container.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				m_menu.securitButton();
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {}

			@Override
			public void mouseExited(MouseEvent arg0) {}

			@Override
			public void mousePressed(MouseEvent arg0) {}

			@Override
			public void mouseReleased(MouseEvent arg0) {}
		});
		
	}
	
	private void onGridDoubleClicked(int x, int y){
		if(game.IsLaunched()) // bloquer si le jeu est lance
			return;
		if(m_menu.getSelectedScriptIndex() != -1){
			String selectedScript = m_menu.getSelectedScriptName();
			if(selectedScript.length() > 0){
				String scriptPath = Tools.getScriptsPath() + selectedScript;
				game.getBattlefield().addTank(new Coordinates(x, y),scriptPath,game);
			}
		}
	}
	
	private void installTooltips(){
		m_menu.installTooltips();
		grid.setToolTipText("Double click on a square to create a tank running the selected script");
		start.setToolTipText("Start the game");
		raz.setToolTipText("Stop the game and delete all tanks");
	}
	
	public static void setMenuEnabled(boolean b) {
		m_menu.setEnabled(b);
	}
	
	@SuppressWarnings("static-access")
	public static void callMenuRefresh() {
		m_menu.refresh();
	}
	
	public static void hideGridPanel(){
		grid.setVisible(false);
	}
	
	public static void hideEditorPanel(){
		editor.getContainer().setVisible(false);
		start.setEnabled(true);
		raz.setEnabled(true);
	}
	
	public static void showGridPanel(){
		grid.setVisible(true);
	}
	
	public static void showEditorPanel(){
		editor.getContainer().setVisible(true);
		start.setEnabled(false);
		raz.setEnabled(false);
	}

}
