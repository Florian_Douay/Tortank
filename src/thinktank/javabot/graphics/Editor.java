package thinktank.javabot.graphics;


import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import thinktank.javabot.physics.Tools;


public class Editor {
	// Private
	
	private static JPanel editor_container 		= new JPanel();
	private static JPanel editor_menu 			= new JPanel();
	private static JTextField nom_fichier 		= new JTextField();
	private static JButton editor_save 			= new JButton("save");
	private static JButton editor_quit			= new JButton("quit");
	private static JTextArea textArea 			= new JTextArea(50, 10);
	private static JScrollPane editor_text		= new JScrollPane(textArea);
	
	
	public Editor()
	{	
	
		nom_fichier.setDocument(new JTextFieldLimit(6));
		// initialisation de la barre de bouton
		GridBagConstraints gc = new GridBagConstraints();
		gc.insets = new Insets(5, 5, 5, 5);
		editor_menu.setLayout(new GridLayout(1,3));
		
		editor_menu.add(nom_fichier );
		editor_menu.add(editor_save );
		editor_menu.add(editor_quit);
		editor_menu.add(editor_text);
		
		// initialisation du container		
		editor_container.setLayout(new BorderLayout());
		editor_container.add("North", editor_menu);
		editor_container.add("Center", editor_text);
		
	
		// initialisation des boutons
		editor_save.addActionListener( new ActionListener(){
		@Override
			public void actionPerformed(ActionEvent e){
				onClickSaveEvent();			
			}
		});
				
		editor_quit.addActionListener(new ActionListener(){
		@Override
			public void actionPerformed(ActionEvent e){
				onClickQuitEvent();
			}
		});
		}
			
	
	public void onClickSaveEvent() {
		
		String file_name_input = nom_fichier.getText();
		
		if(file_name_input.isEmpty()){
			JOptionPane.showMessageDialog(null, "The script name cannot be empty","Impossible to save",JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		
		if(file_name_input.length() > 6){
			JOptionPane.showMessageDialog(null, "The script name cannot be longer than 5 characters","Impossible to save",JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		
		String file_name_final = file_name_input + ".py";
		
		File file = new File(Tools.getScriptsPath() + file_name_final);
		
		if(file.exists()){
			//Custom button text
			Object[] options = {"Yes",
			                    "No"};
			int n = JOptionPane.showOptionDialog(null,
			    "Etes-vous sur de vouloir écraser le script " + file_name_final + " ?",
			    "Confimation",
			    JOptionPane.YES_NO_OPTION,
			    JOptionPane.QUESTION_MESSAGE,
			    null,
			    options,
			    options[1]);
			
			if ( n == JOptionPane.NO_OPTION ){
				return;
			}
		}
		
		BufferedWriter bufferToFile;
        try {
            bufferToFile = new BufferedWriter(new FileWriter(file,false));
            textArea.write(bufferToFile);
            bufferToFile.close();
            JOptionPane.showMessageDialog(null, "Script saved successfully","Script Saved",JOptionPane.INFORMATION_MESSAGE);
            // true for rewrite, false for override

        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "An error occured during the save operation");
            e.printStackTrace();
        }
        
        // refresh du menu
        MainWindow.callMenuRefresh();
    }
	
	public void loadFile(String filename) {
		
	}
	
	private void onClickQuitEvent(){
		
		MainWindow.showGridPanel();	
		MainWindow.hideEditorPanel();
		MainWindow.setMenuEnabled(true);
		Menu.refresh();
		textArea.setText("");
		nom_fichier.setText("");
		
	}
	
	public JPanel getContainer() 
	{
		return editor_container;
	}
	
	public static JTextArea getJTextArea(){
		return textArea;
	}
	
	public static JTextField getJTextField(){
		return nom_fichier;
	}
}
	
