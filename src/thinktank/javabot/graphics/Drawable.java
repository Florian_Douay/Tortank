package thinktank.javabot.graphics;

public interface Drawable {

	void accept(DrawableVisitor visitor);
}
