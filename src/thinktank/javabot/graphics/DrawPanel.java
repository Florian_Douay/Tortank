package thinktank.javabot.graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JPanel;

import thinktank.javabot.physics.Game;
import thinktank.javabot.physics.Placeable;
import thinktank.javabot.physics.Tank;
import thinktank.javabot.physics.TypeObjectTT;

@SuppressWarnings("serial")
public class DrawPanel extends JPanel {

	Game game;
	GraphicDrawer graphicTools;
	
	public DrawPanel(Game game){
		super();
		this.game =  game;
		this.graphicTools = new GraphicDrawer();
	}
	
	public void paintComponent(Graphics g) 
    {     
		graphicTools.setGraphics(g);
		
		g.setColor(Color.black);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		int lx = 42;
		int ly = 24;
		
		for(int i = 0; i < lx; i++){
			for(int j = 0; j< ly; j++){	
				
				Placeable content = game.getBattlefield().getContentAtLocation(i,j);

				if(content.getType() != TypeObjectTT.TANK){
					content.accept(graphicTools);
				}
			}
		}
		
		CopyOnWriteArrayList<Tank> tanks = game.getBattlefield().getTanksArray();
		
		for(Tank tank : tanks){
			tank.accept(graphicTools);
		}
		
    }

}
