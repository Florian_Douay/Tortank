package thinktank.javabot.graphics;

import java.awt.Image;
import java.util.ArrayList;

import thinktank.javabot.physics.Direction;
import thinktank.javabot.physics.Tools;

public class ProjectileSpriteSet {
	SpriteSet spriteSet;
	
	public ProjectileSpriteSet(){
		
		ArrayList<String> strArray = new ArrayList<String>();
		
		String ammoTexturesPath = Tools.getAmmoTexturesPath();
		
		strArray.add(ammoTexturesPath + "ammo_top1.png");
		strArray.add(ammoTexturesPath + "ammo_bot1.png");
		strArray.add(ammoTexturesPath + "ammo_left1.png");
		strArray.add(ammoTexturesPath + "ammo_right1.png");
		
		this.spriteSet = new SpriteSet(strArray);
	}
	
	public Image getImg(Direction.type direction) {
		
		int index = getIndexFromDirection(direction);
		
		return this.spriteSet.getImg(index);
	}
	
	private int getIndexFromDirection(Direction.type type){
		switch (type){
			case TOP:
				return 0;
			case BOTTOM:
				return 1;
			case LEFT:
				return 2;
			case RIGHT:
				return 3;
				
			//never happens
			default:
				return 0;
		}
	}
}
