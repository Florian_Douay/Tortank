package thinktank.javabot.graphics;

import java.awt.Image;
import java.util.ArrayList;

import thinktank.javabot.physics.Direction;
import thinktank.javabot.physics.Tools;

public class TankSpriteSet {
	
	SpriteSet spriteSet;
	
	public TankSpriteSet(){
		
		ArrayList<String> strArray = new ArrayList<String>();
		
		String tankTexturesPath = Tools.getTankTexturesPath();
		
		strArray.add(tankTexturesPath + "tank_top/canon_top1.png");
		strArray.add(tankTexturesPath + "tank_top/canon_bot1.png");
		strArray.add(tankTexturesPath + "tank_top/canon_left1.png");
		strArray.add(tankTexturesPath + "tank_top/canon_right1.png");
		
		strArray.add(tankTexturesPath + "tank_bot/canon_top1.png");
		strArray.add(tankTexturesPath + "tank_bot/canon_bot1.png");
		strArray.add(tankTexturesPath + "tank_bot/canon_left1.png");
		strArray.add(tankTexturesPath + "tank_bot/canon_right1.png");
		
		strArray.add(tankTexturesPath + "tank_left/canon_top1.png");
		strArray.add(tankTexturesPath + "tank_left/canon_bot1.png");
		strArray.add(tankTexturesPath + "tank_left/canon_left1.png");
		strArray.add(tankTexturesPath + "tank_left/canon_right1.png");
		
		strArray.add(tankTexturesPath + "tank_right/canon_top1.png");
		strArray.add(tankTexturesPath + "tank_right/canon_bot1.png");
		strArray.add(tankTexturesPath + "tank_right/canon_left1.png");
		strArray.add(tankTexturesPath + "tank_right/canon_right1.png");
		
		this.spriteSet = new SpriteSet(strArray);
	}
	
	public Image getImg(Direction.type tankDirection, Direction.type canonDirection) {
		
		int index1 = getIndexFromDirection(tankDirection);
		int index2 = getIndexFromDirection(canonDirection);
		
		return this.spriteSet.getImg((index1 * 4) + index2);
	}
	
	private int getIndexFromDirection(Direction.type type){
		switch (type){
			case TOP:
				return 0;
			case BOTTOM:
				return 1;
			case LEFT:
				return 2;
			case RIGHT:
				return 3;
				
			//never happens
			default:
				return 0;
		}
	}

}
