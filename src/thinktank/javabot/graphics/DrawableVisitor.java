package thinktank.javabot.graphics;

import thinktank.javabot.physics.Tank;
import thinktank.javabot.physics.Projectile;
import thinktank.javabot.physics.Wall;
import thinktank.javabot.physics.Void;

public interface DrawableVisitor {
	
	public void visit(Tank tank);
	
	public void visit(Projectile projectile);
	
	public void visit(Wall wall);
	
	public void visit(Void voidInstance);

}
