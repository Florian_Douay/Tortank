package thinktank.javabot.graphics;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JButton;

import thinktank.javabot.physics.Tools;

public class Menu 
{
	// Private

	private static JPanel m_container = new JPanel();
	private static JPanel m_button_bar = new JPanel();
	private static JButton m_but_create;
	private static JButton m_but_edit;
	private static JButton m_but_delete;
	private static JList<String> m_script_list = new JList<String>(new DefaultListModel<String>());

	
	// Public

	public Menu(){
		
		String iconsPath = Tools.getIconsPath();
		
		ImageIcon add		= new ImageIcon(iconsPath + "add.png");
		ImageIcon edit		= new ImageIcon(iconsPath + "edit.png");
		ImageIcon delete	= new ImageIcon(iconsPath + "delete.png");
		
		m_but_create = new JButton("",add);
		m_but_edit = new JButton("",edit);
		m_but_delete = new JButton("",delete);

		// initialisation de la barre de bouton
		m_button_bar.setLayout(new GridLayout(1,3));
		m_button_bar.add(m_but_create);
		m_button_bar.add(m_but_edit);
		m_button_bar.add(m_but_delete);
		
		// initialisation du container		
		m_container.setLayout(new BorderLayout());
		m_container.add("North", m_button_bar);
		m_container.add("Center", m_script_list);
		
		// initialisation de la list
		m_script_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		refresh();
		
		// initialisation des boutons
		m_but_create.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				onButAddEvent();			
			}
		});
		m_but_edit.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				onButEditEvent();
			}
		});
		m_but_delete.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				onButDeleteEvent();
			}
		});
	}


	public static void refresh() {
		String[] directoryContent;

		// obtention de la liste des scripts
		directoryContent = new java.io.File(Tools.getScriptsPath()).list(new FilterFilePy());
		java.util.Arrays.sort(directoryContent);
		for(int i=0 ; i<directoryContent.length ; i++)
		{
			String s = directoryContent[i];
			directoryContent[i] = s.substring(0, s.length()-3);
		}
		
		// stockage dans m_tank_list
		m_script_list.setListData(directoryContent);		
	}
	
	public void securitButton()
	{
		if(m_script_list.getSelectedIndex() == -1)
		{
			m_but_edit.setEnabled(false);
			m_but_delete.setEnabled(false);
		}
		else
		{
			m_but_edit.setEnabled(true);
			m_but_delete.setEnabled(true);
		}
	}
	
	public void installTooltips(){
		m_but_create.setToolTipText("Create a new script and launch editor");
		m_but_edit.setToolTipText("Launch editor for the selected script");
		m_but_delete.setToolTipText("Remove the selected script");
		m_script_list.setToolTipText("Click to select a script");
	}
	
	public void setEnabled(boolean state) {
		this.setEnabledButton(state);
		m_script_list.setEnabled(state);
	}
	
	public void setEnabledButton(boolean state) {
		m_but_create.setEnabled(state);
		m_but_edit.setEnabled(state);
		m_but_delete.setEnabled(state);
	}
	
	
	// Getter and Setter
	
	public JPanel getContainer(){
		return m_container;
	}
	
	public String getSelectedScriptName(){
		int index = m_script_list.getSelectedIndex();
		if(index < 0)
			return new String("");
		return m_script_list.getSelectedValue().concat(".py");
	}
	
	public int getSelectedScriptIndex()
	{
		return m_script_list.getSelectedIndex();
	}
	
	public void getScriptOnClick(){
		
		String choice = getSelectedScriptName();
		
		String fichier = Tools.getScriptsPath() +choice;
		
		Editor.getJTextField().setText(m_script_list.getSelectedValue());
		
		
		FileReader flux= null;
		BufferedReader input= null;
		 String str;
		       try{ 
		           flux= new FileReader(fichier); 
		           input= new BufferedReader(flux);
		
		        while((str=input.readLine())!=null)
		        {
		          Editor.getJTextArea().append(str+"\n");
		          }
		         }catch (IOException e)
		         {
		             System.out.println("Impossible d'ouvrir le fichier : " +e.toString()); 
		         }
		      finally{
		            try {
		            	input.close();
		                flux.close();
		                
		            } catch (IOException ex) {
		              System.out.println("Impossible de fermer le fichier : " +ex.toString());   
		            }
		}
	
	}
	
	
	// Private methods
	
	private void onButAddEvent(){		
		MainWindow.hideGridPanel();
		MainWindow.showEditorPanel();
		this.setEnabled(false);
	}
	
	private void onButEditEvent(){
		if(m_script_list.getSelectedIndex() < 0) // si aucune selection
			return;
		
		MainWindow.hideGridPanel();
		MainWindow.showEditorPanel();
		this.setEnabled(false);
		getScriptOnClick();
	}
	
	private void onButDeleteEvent(){
		if(m_script_list.getSelectedIndex() < 0) // si aucune selection
			return;
		
		//Custom button text
		Object[] options = {"Yes",
		                    "No"};
		int n = JOptionPane.showOptionDialog(null,
		    "Etes-vous sur de vouloir supprimer definitivement ce script ?",
		    "Confimation",
		    JOptionPane.YES_NO_CANCEL_OPTION,
		    JOptionPane.QUESTION_MESSAGE,
		    null,
		    options,
		    options[1]);
	
		if ( n == JOptionPane.OK_OPTION ){
			File targetFile = new File(Tools.getScriptsPath() + getSelectedScriptName());
			targetFile.delete();
		}
		// refresh de la liste
		refresh(); 
	}
	
	

}