package thinktank.javabot.graphics;

import java.awt.Image;
import java.util.ArrayList;

public class SpriteSet {
	
	Sprite sprites[]; 
	
	public SpriteSet(ArrayList<String> strArray) {
		
		sprites = new Sprite[strArray.size()];
		
		for (int i = 0 ; i < strArray.size() ; i++){
			sprites[i] = new Sprite(strArray.get(i));
		}
    }

	public Image getImg(int num) {
		return sprites[num].getImg();
	}

}
