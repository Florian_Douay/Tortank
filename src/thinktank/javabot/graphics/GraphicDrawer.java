package thinktank.javabot.graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import thinktank.javabot.physics.Direction;
import thinktank.javabot.physics.Projectile;
import thinktank.javabot.physics.Tank;
import thinktank.javabot.physics.Tools;
import thinktank.javabot.physics.Void;
import thinktank.javabot.physics.Wall;

public class GraphicDrawer implements DrawableVisitor {
	
	private Graphics graphics;
	
	private int tailleCase;
	private TankSpriteSet tankSpriteSet;
	private ProjectileSpriteSet projectileSpriteSet;
	private Sprite wallSprite;
	private Sprite floorSprite;
	
	public GraphicDrawer(){
		
		this.graphics = null;
		this.tailleCase = 24;
		this.tankSpriteSet = new TankSpriteSet();
		this.projectileSpriteSet = new ProjectileSpriteSet();
		this.wallSprite = new Sprite(Tools.getWallTexturesPath() + "wall1.jpeg");
		this.floorSprite = new Sprite(Tools.getGroundTexturesPath() + "ground1.jpeg");
	}
	
	public void setGraphics(Graphics graphics){
		this.graphics = graphics;
	}
	
	public void draw(Drawable drawable){
		drawable.accept(this);
	}
	
	public void visit(Tank tank){
		
		int x = tank.getCoordX() * tailleCase;
		int y = tank.getCoordY() * tailleCase;
		
		int vie = Integer.valueOf(tank.getCurrentHealthPoints());
		
		Direction.type tankDirectionType = tank.getDirection().getType();
		Direction.type canonDirectionType = tank.getCanon().getDirection().getType();
		
		Image img = tankSpriteSet.getImg(tankDirectionType,canonDirectionType);
		
		graphics.drawImage(img,x,y,tailleCase,tailleCase,null);
		graphics.setColor(Color.CYAN);
		graphics.fillRect(x, y-10, 24*vie/100, 5);
		graphics.setColor(Color.WHITE);
		graphics.drawRect(x, y-10, 24, 5);
		
		String algoName = Tools.extractAlgoNameFromPath(tank.getAi().getFilePath());
		
		//gross implementation to try to align center tank's name with tank itself
		
		int nameLength = algoName.length();
		int posXToUse = x;
		
		if (nameLength == 1)
			posXToUse += 8;
		else if (nameLength == 2)
			posXToUse += 3;
		if (nameLength == 4)
			posXToUse -= 2;
		if (nameLength == 5)
			posXToUse -= 4;
		if (nameLength >= 6)
			posXToUse -= 6;

		graphics.drawString(algoName,posXToUse, y+35);
	}

	public void visit(Projectile projectile){
		
		int x = projectile.getCoordX() * tailleCase;
		int y = projectile.getCoordY() * tailleCase;
		
		Direction.type projectileDirectionType = projectile.getDirection().getType();
		
		Image img = projectileSpriteSet.getImg(projectileDirectionType);
		
		graphics.drawImage(img,x,y,tailleCase,tailleCase,null);
	}
	
	public void visit(Wall wall){
		
		int x = wall.getCoordX() * tailleCase;
		int y = wall.getCoordY() * tailleCase;
		
		graphics.drawImage(wallSprite.getImg(),x,y,tailleCase,tailleCase,null);
	}
	
	public void visit(Void voidInstance){
		
		int x = voidInstance.getCoordX() * tailleCase;
		int y = voidInstance.getCoordY() * tailleCase;
		
		graphics.drawImage(floorSprite.getImg(),x,y,tailleCase,tailleCase,null);
	}
	
}
