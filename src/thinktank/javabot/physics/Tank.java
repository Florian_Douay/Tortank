package thinktank.javabot.physics;

import thinktank.javabot.graphics.DrawableVisitor;
import thinktank.javabot.intelligences.Action;
import thinktank.javabot.intelligences.Intelligence;
import thinktank.javabot.intelligences.IntelligencesContainer;
import thinktank.javabot.sensors.Radar;
import thinktank.javabot.sensors.Sensors;

public class Tank extends Movable{

	private int healthPoints;
	private Ammo ammo;
	private Intelligence ai;
	private Sensors sensor;
	private Canon canon;

	protected Tank(Coordinates coord, BattleField map, String filepath, Game game) {

		super(coord,map);
		this.healthPoints = 100;
		this.ammo = new Ammo();
		setDirection(new Direction(Direction.type.BOTTOM));
		Intelligence intel = new Intelligence(filepath,this);
		this.ai = IntelligencesContainer.getInstance().addIntelligence(intel);
		this.ai.start();
		this.sensor = new Radar(this,game);
		this.canon = new Canon(Direction.type.BOTTOM);
	}
	
	public Intelligence getAi() {
		return ai;
	}

	public Sensors getSensor() {
		return sensor;
	}

	public void setSensor(Sensors sensor) {
		if (sensor != null)
			this.sensor = sensor;
	}
	
	public Ammo getAmmo(){
		return ammo;
	}
	
	public void setAmmo(Ammo ammo){
		if (ammo != null)
			this.ammo = ammo;
	}
	
	public Canon getCanon() {
		return canon;
	}

	public int getCurrentHealthPoints(){
		return healthPoints;
	}

	protected void receiveDamages(int damages){
		this.healthPoints = healthPoints - damages;
		
		if(healthPoints <= 0){ 
			healthPoints = 0;
			destroy();
		}
	}
	
	public TypeObjectTT getType(){
		return TypeObjectTT.TANK;
	}
	
	protected void shootAnAmmo() 
	/**
 	* crée et lance un Projectile dans la direction du canon du tank, ou reduit le temps d'attente avant de pouvoir tirer
 	*/
	{
		
		int coordXToInsertAmmo = getCoordX() + canon.getDirection().getDx();
		int coordYToInsertAmmo = getCoordY() + canon.getDirection().getDy();
		
		if (ammo.getCooldownAction() > 0){
			ammo.lowerCooldownAction();
			return;
		}
		
		if (getBattleField().isSquareFree(getCoordX() + canon.getDirection().getDx(),
					 					  getCoordY() + canon.getDirection().getDy())){
				Direction direction = new Direction(canon.getDirection());
				Projectile projectile = ammo.createProjectile(getCoordX() + canon.getDirection().getDx(),
													          getCoordY() + canon.getDirection().getDy(),
													          direction,
													          getBattleField()
													          );
				getBattleField().addProjectile(projectile);
		 }
		else{
			if(getBattleField().getContentAtLocation(coordXToInsertAmmo, coordYToInsertAmmo).getType() == TypeObjectTT.TANK){
				Tank target = (Tank)getBattleField().getContentAtLocation(coordXToInsertAmmo, coordYToInsertAmmo);
				target.receiveDamages(ammo.getDamages());
			}
			else{
				ammo.lowerCooldownAction();
			}
		}
	}

	protected void destroy() {
		getBattleField().destroyObjectFromBattlefield(getCoordX(), getCoordY());
		setIsStillExisting(false);
		getBattleField().removeTank(this);
	}

	protected void doAction(Action action){
		if (getCooldownAction() > 0) {
			setCooldownAction(getCooldownAction() - 1);
			return;
		}
		
		if (healthPoints <= 0)
			return;

		switch (action) {
			case moveForward:
				forward();
				setCooldownAction(0);
				break;
	
			case moveBackward:
				backward();
				setCooldownAction(0);
				break;
	
			case shoot:
				shootAnAmmo();
				setCooldownAction(0);
				break;
	
			case turnClockwise:
				getDirection().turnOnTheRight();
				canon.getDirection().turnOnTheRight();
				setCooldownAction(0);
				break;
	
			case turnCounterClockwise:
				getDirection().turnOnTheLeft();
				canon.getDirection().turnOnTheLeft();
				setCooldownAction(0);
				break;
				
			case turnCanonClockwise:
				canon.turnCanonClockwise();
				setCooldownAction(0);
				break;
				
			case turnCanonCounterClockwise:
				canon.turnCanonCounterClockwise();
				setCooldownAction(0);
				break;
	
			default:
				break;
		}

	}
	
	protected void unlockAI(){
		if (getCooldownAction() == 0) {
			ai.unlockForNextInstruction();
		}
	}

	public void getAndDoAction(){
		if (getCooldownAction() > 0) {
			setCooldownAction(getCooldownAction() - 1);
			return;
		}
		
		Action actionToDo = ai.getAction();
		
		if(actionToDo != null){
			if(getBattleField().isDisplayEnabled()){
				System.out.println("tank: "+this.getAi().getFilePath()+" act: "+actionToDo);
			}
		}
		else{
			actionToDo = Action.noAction;
		}
		doAction(actionToDo);
		
	}
	
	public void accept(DrawableVisitor visitor){
		visitor.visit(this);
	}

}
