package thinktank.javabot.physics;

import thinktank.javabot.physics.BattleField;;

/**
 * Classe pour tester la partie physique
 *
 */
public class TestPhysics {

	public static void affiche(Game game){
		BattleField map = game.getBattlefield();
		
		for(int j=0; j < map.getBattlefieldHeight();j++){
			System.out.println();
			for(int i=0; i < map.getBattlefieldWidth();i++)
				switch (game.getBattlefield().getContentAtLocation(i,j).getType()) {
				case VOID:
					System.out.print('a');
					break;
				case WALL:
					System.out.print('l');
					break;

				default:
					System.out.print(game.getBattlefield().getContentAtLocation(i,j).getType());
					break;
				}
			}
		System.out.println();
	}
	
	public static void testProjectile(){
		Game game = new Game(10, 10);
		BattleField map = game.getBattlefield();
		Tank t = map.addTank(new Coordinates(1,8),Tools.getScriptsPath() + "tank1.py",game);
		t.getDirection().turnOnTheRight();
		t.getDirection().turnOnTheRight();
		game.getBattlefield().addTank(new Coordinates(1,1),Tools.getScriptsPath() + "tank1.py",game);
		game.getBattlefield().addTank(new Coordinates(2,1),Tools.getScriptsPath() + "tank1.py",game);
		game.getBattlefield().addTank(new Coordinates(3,1),Tools.getScriptsPath() + "tank1.py",game);
		affiche(game);
		while(true){
			game.iterationActions();
			affiche(game);
			if(game.getBattlefield().getTanksArray().isEmpty()) break;
		}
	}
	
	
	public static void test(){
		Game game = new Game(10, 10);
		BattleField map = game.getBattlefield();
		map.addTank(Tools.getRandomXYAvailable(map),Tools.getScriptsPath() + "tankytourne2.py",game);
		map.addTank(Tools.getRandomXYAvailable(map),Tools.getScriptsPath() + "tankytourne2.py",game);
		affiche(game);
		while(true){
			game.iterationActions();
			affiche(game);
			if(game.getBattlefield().getTanksArray().isEmpty()) break;
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		test();
	}
}
