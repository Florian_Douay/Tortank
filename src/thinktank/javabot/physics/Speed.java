package thinktank.javabot.physics;


public class Speed {
	
	/* Not really used at the moment, because it needs a deep logic refactor.
	 * Actually instructions frequency are slowed by cooldown variable.
	 * The problem of this class is its value can't be used because in the current system
	 * because the cooldown is the inverse of the speed */
	
	private int value;
	
	protected Speed() {
		this.value = 0;
	}

	protected int getValue() {
		return value;
	}

	protected void setValue(int newValue) {
		if(newValue >= 0)
			this.value = newValue;
	}

}
