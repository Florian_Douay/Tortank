package thinktank.javabot.physics;

import java.util.Random;

public class Tools {
	
	public static Coordinates getRandomXYAvailable(BattleField battlefield){
		
		int x = Alea(1, battlefield.getBattlefieldWidth() - 1);
		int y = Alea(1, battlefield.getBattlefieldHeight() - 1);
		
		while (!battlefield.isSquareFree(x,y)) {
			x = Alea(1, battlefield.getBattlefieldWidth() - 1);
			y = Alea(1, battlefield.getBattlefieldHeight() - 1);
		}
		
		return new Coordinates(x,y);
	}
	
	public static boolean areCoordinatesInsideBattlefield(int x, int y, BattleField battlefield){
		
		return (x >= 0 && y >= 0 && x < battlefield.getBattlefieldWidth() && y < battlefield.getBattlefieldHeight());
	}
	
	public static int Alea(int valeurMin, int valeurMax) 
	/**
 	* renvoie un nombre entre borne inférieure et borne supérieure
	* @param valeurMin borne inférieure
 	* @param valeurMax borne supérieure
 	*/
	{
		Random r = new Random();
		return valeurMin + r.nextInt(valeurMax-valeurMin);
	}
	
	public static String extractAlgoNameFromPath(String path){
		
		int indexDirectory = path.lastIndexOf('/', path.length()-1);

		String fileName = path.substring(indexDirectory+1);

		int indexExtension = fileName.lastIndexOf(".py",fileName.length()-1);

		return fileName.substring(0,indexExtension);
	}
	
	private static String ressourcesPath(){
		return new String("ressources/");
	}
	
	private static String texturesPath(){
		return new String(ressourcesPath() + "textures/");
	}
	
	public static String getScriptsPath(){
		return new String(ressourcesPath() + "scripts/");
	}
	
	public static String getIconsPath(){
		return new String(ressourcesPath() + "icons/");
	}
	
	public static String getAmmoTexturesPath(){
		return new String(texturesPath() + "ammo_textures/");
	}
	
	public static String getGroundTexturesPath(){
		return new String(texturesPath() + "ground_textures/");
	}
	
	public static String getTankTexturesPath(){
		return new String(texturesPath() + "tanks_textures/tank_blue/");
	}
	
	public static String getWallTexturesPath(){
		return new String(texturesPath() + "wall_textures/");
	}

}
