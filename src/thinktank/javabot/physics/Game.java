package thinktank.javabot.physics;

import java.util.concurrent.CopyOnWriteArrayList;

import thinktank.javabot.graphics.DrawPanel;

public class Game extends Thread {

	private BattleField battlefield;
	private DrawPanel grid;
	private boolean isLaunched;

	public Game(int width, int height){
		battlefield = new BattleField(width, height);
		isLaunched = false;		
		start();
	}
	
	public BattleField getBattlefield(){
		return battlefield;
	}
	
	public void setView(DrawPanel grid){
		this.grid = grid;
	}
	
	public void tryUpdateView(){
		if(this.grid != null){
			this.grid.repaint();
		}
	}
	
	public synchronized void startGame(){
		isLaunched = true;
		notifyAll();
	}
	
	public void stopGame(){
		isLaunched = false;
	}
	
	public boolean IsLaunched() {
		return isLaunched;
	}
	
	public void clearBattleField(){
		int width = battlefield.getBattlefieldWidth();
		int height = battlefield.getBattlefieldHeight();
		
		battlefield = new BattleField(width, height);
	}

	//refactoring iter
	/**
 	* lance la prochaine action de tout les éléments du Terrain
	 * @throws InterruptedException 
 	*/
	public synchronized void iterationActions(){
		
		while(!isLaunched){
			tryUpdateView();
			try{
				wait(500);
			} catch(InterruptedException e){
				return;
			}
		}
		
		//on traite en premier les projectiles
		CopyOnWriteArrayList<Projectile> projectileArray = battlefield.getProjectilesArray();
		if(!projectileArray.isEmpty()){
			for(Projectile p : projectileArray){
				p.forward();
			}
		}
		
		//on exécute en deuxième les actions des tanks
		CopyOnWriteArrayList<Tank> tanks = battlefield.getTanksArray();
		for(Tank t : tanks){
			t.getAndDoAction();
		}
		
		//on débloque les threads des script pour mettre à jour la prochaine instruction à exécuter
		for(Tank t : battlefield.getTanksArray()){
			t.unlockAI();
			t.getAmmo().lowerCooldownAction();
		}
		
		try {
			//modifie la vitesse du jeu (100)
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 *  Thread dédié à l'exécution de la partie */
	public void run(){
		
		while(true){
			iterationActions();
			tryUpdateView();
		}
	}
	
}
