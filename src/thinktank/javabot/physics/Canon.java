package thinktank.javabot.physics;

public class Canon {
	
	private Direction direction;
	
	
	protected Canon(Direction.type directionType){
		this.direction = new Direction(directionType);
	}
	
	public Direction getDirection() {
		return direction;
	}
	
	protected void turnCanonClockwise(){
		direction.turnOnTheRight();
	}
	
	protected void turnCanonCounterClockwise(){
		direction.turnOnTheLeft();
	}
		
}
