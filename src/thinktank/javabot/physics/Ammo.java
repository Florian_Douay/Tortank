package thinktank.javabot.physics;


public class Ammo {
	
	private int cooldownAction;
	private int damages;
	
	protected Ammo(){
		this.cooldownAction = 0;
		this.damages = 20;
	}

	protected Projectile createProjectile(int x, int y, Direction direction, BattleField map){
		this.cooldownAction = 3;
		return new Projectile(x, y, direction, map, damages);
	}
	
	protected int getCooldownAction() {
		return cooldownAction;
	}
	
	protected void lowerCooldownAction() {
		if (this.cooldownAction > 0){
			this.cooldownAction--;
		}
	}
	
	public int getDamages() {
		return damages;
	}

}
