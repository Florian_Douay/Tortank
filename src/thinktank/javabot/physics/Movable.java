package thinktank.javabot.physics;


public abstract class Movable extends Placeable{
	
	private Speed speed;
	private int cooldownAction;
	
	Movable(Coordinates coord, BattleField map){
		super(map,coord);
		this.setSpeed(new Speed());
		this.cooldownAction = 0;
	}
	
	protected Speed getSpeed() {
		return speed;
	}

	protected void setSpeed(Speed speed) {
		this.speed = speed;
	}

	protected int getCooldownAction() {
		return cooldownAction;
	}
	
	protected void setCooldownAction(int cooldown) {
		this.cooldownAction = cooldown;
	}
	
	protected void forward(){
		Coordinates oldCoord = new Coordinates(this.coord);
		
		if( map.TestAndSetCase(this, coord.getX() + direction.getDx(), coord.getY() + direction.getDy()))
		{
			coord.setX(coord.getX() + direction.getDx());
			coord.setY(coord.getY() + direction.getDy());
			map.destroyObjectFromBattlefield(oldCoord.getX(), oldCoord.getY());
		}
	}
	
	protected void backward(){
		Coordinates oldCoord = new Coordinates(this.coord);
		
		if( map.TestAndSetCase(this, coord.getX() - direction.getDx(), coord.getY() - direction.getDy()))
		{
			coord.setX(coord.getX() - direction.getDx());
			coord.setY(coord.getY() - direction.getDy());
			map.destroyObjectFromBattlefield(oldCoord.getX(), oldCoord.getY());
		}
	}
	
}
