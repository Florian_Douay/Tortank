package thinktank.javabot.physics;

import java.util.concurrent.CopyOnWriteArrayList;

public class BattleField {
	
	private Placeable battlefieldSquaresContent[][];
	private CopyOnWriteArrayList<Tank> tanksArray = new CopyOnWriteArrayList<Tank>();
	private CopyOnWriteArrayList<Projectile> projectilesArray = new CopyOnWriteArrayList<Projectile>();
	private int battlefieldWidth;
	private int battlefieldHeight;
	private boolean isDisplayEnabled = false;
	
	protected BattleField(int width, int height)
	{
		this.battlefieldWidth = width;
		this.battlefieldHeight = height;
		
		battlefieldSquaresContent = new Placeable [battlefieldWidth][battlefieldHeight];

		/* Initialize limit walls of the battlefield */
		for (int i = 0; i< battlefieldWidth; i++){
			for (int j = 0; j< battlefieldHeight; j++){
				if(i == 0 || i == battlefieldWidth-1  || j == 0 || j == battlefieldHeight-1){
					createWall(i,j);
				}
				else{
					setVoid(i,j);
				}
			}
		}
		
		/* Initialize some walls on the battlefield */
		for(int i = 5; i < 10; i++){
			createWall(i,5);
		}
		for(int i = 18; i < 25; i++){
			createWall(i,15);
		}
		for(int i = 5; i < 10 ; i++){
			createWall(30,i);
		}
		for(int i = 28; i < 33; i++){
			createWall(i,20);
		}
		for(int i = 10; i < 15 ; i++){
			createWall(8,i);
		}
	}

	public ObjetTT[][] getBattlefieldSquaresContent(){
		return battlefieldSquaresContent;
	}
	
	public CopyOnWriteArrayList<Tank> getTanksArray(){
		return tanksArray;
	}
	
	public CopyOnWriteArrayList<Projectile> getProjectilesArray(){
		return projectilesArray;
	}
	
	public int getBattlefieldWidth() {
		return battlefieldWidth;
	}

	public int getBattlefieldHeight() {
		return battlefieldHeight;
	}
	
	public boolean isDisplayEnabled() {
		return isDisplayEnabled;
	}
	
	protected void setDisplayEnabled(boolean bool) {
		isDisplayEnabled = bool;
	}
	
	protected void addPlaceableObject(int x, int y, Placeable obj){
		battlefieldSquaresContent[x][y] = obj;
	}
	
	protected void destroyObjectFromBattlefield(int x, int y){
		setVoid(x,y);
	}
	
	public Placeable getContentAtLocation(int x, int y){
		if(Tools.areCoordinatesInsideBattlefield(x, y, this))
			return battlefieldSquaresContent[x][y];
		return null;
	}
	
	public Tank addTank(Coordinates coord ,String filepath, Game game){
		if(isSquareFree(coord.getX(), coord.getY())){
			Tank t = new Tank(coord,this,filepath,game);
			tanksArray.add(t);
			addPlaceableObject(t.getCoordX(), t.getCoordY(), t);
			return t;
		}
		else{
			if(isDisplayEnabled()){
				System.out.println("Impossible d'ajouter le tank. La position demandée est déjà occupée");
			}
			return null;
		}
	}
	
	protected void removeTank(Tank tank){
		tanksArray.remove(tank);
	}
	
	protected void addProjectile(Projectile proj){
		projectilesArray.add(proj);
		addPlaceableObject(proj.getCoordX(), proj.getCoordY(), proj);
	}

	protected void removeProjectile(Projectile proj){
		projectilesArray.remove(proj);
	}
	
	/** Create a wall at specified location if it is in battlefield and if it is free (void) **/
	protected void createWall(int x, int y){
		if(isSquareFree(x,y))
			battlefieldSquaresContent[x][y] = new Wall(this,new Coordinates(x,y));
	}
	
	/** Set specified location as free (void) if it is in battlefield **/
	private void setVoid(int x, int y){
		if(Tools.areCoordinatesInsideBattlefield(x, y, this))
			battlefieldSquaresContent[x][y] = new Void(this,new Coordinates(x,y));
	}

	protected boolean isSquareFree(int x, int y){
		ObjetTT obj = getContentAtLocation(x,y);
		
		if (obj != null){
			return obj.getType() == TypeObjectTT.VOID ;
		}else{
			return true;
		}
	}

	protected boolean TestAndSetCase(Movable mob, int newX, int newY)
	{
		int x = mob.getCoordX();
		int y = mob.getCoordY();
		if(isSquareFree(newX, newY)){
			destroyObjectFromBattlefield(x,y);
			battlefieldSquaresContent[newX][newY] = mob;
			return true;
		}
		if(mob.getType() == TypeObjectTT.PROJECTILE){
			
			if(mob.isStillExisting() == false){
				mob.destroy();
				return true;
			}
			
			if(getContentAtLocation(newX,newY).getType() == TypeObjectTT.WALL ){ // un mur
				mob.destroy();
				return true;
			}
			if(getContentAtLocation(newX,newY).getType() == TypeObjectTT.PROJECTILE)
			{
				Projectile p =((Projectile) getContentAtLocation(newX, newY));
				mob.destroy();
				p.setIsStillExisting(false);
				return true;
			}
			
			if(getContentAtLocation(newX,newY).getType() == TypeObjectTT.TANK)
			{
				mob.destroy();
				((Tank) getContentAtLocation(newX, newY)).receiveDamages(((Projectile)mob).getProjectileDamages());
				return true;
			}
		}
		
		if(mob.getType() == TypeObjectTT.TANK){
			if(getContentAtLocation(newX,newY).getType() == TypeObjectTT.PROJECTILE)
			{
				((Tank) mob).receiveDamages(((Projectile)getContentAtLocation(newX, newY)).getProjectileDamages());
				((Projectile) getContentAtLocation(newX, newY)).destroy();
				return true;
			}
		}
		
		return false;
	}

}
