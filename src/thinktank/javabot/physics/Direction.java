package thinktank.javabot.physics;

public class Direction {
	/**
 	* Class that allows to know current direction and to change it (90 degrees by 90 degrees)
 	*/

	private int dx;
	private int dy;
	
	public enum type{
		TOP,
		RIGHT,
		BOTTOM,
		LEFT
	}
	
	protected Direction (Direction.type directionType){
		switch (directionType) {
		case TOP:
			this.dx = 0;
			this.dy = -1;
			break;

		case RIGHT:
			this.dx = 1;
			this.dy = 0;
			break;

		case BOTTOM:
			this.dx = 0;
			this.dy = 1;
			break;

		case LEFT:
			this.dx = -1;
			this.dy = 0;
			break;

		default:
			//todo error
			break;
		}
	}
	
	public Direction (Direction direction){
		this.dx = direction.dx;
		this.dy = direction.dy;
	}

	public int getDx () { return dx; }

	public int getDy () { return dy; }

	public void turnOnTheRight() {
		int tmp = dx;
		dx = dx*0 + dy*-1;
		dy = tmp*1 + dy*0; 
	}
	
	public void turnOnTheLeft() {
		int tmp = dx;
		dx = dx*0 + dy*1;
		dy = tmp*-1 + dy*0; 
	}
	
	public Direction.type getType(){
		
		if (this.dy == -1)
			return type.TOP;
		
		else if (this.dx == 1)
			return type.RIGHT;

		else if (this.dy == 1)
			return type.BOTTOM;

		else if (this.dx == -1)
			return type.LEFT;
		
		//never happens
		return type.TOP;
	}

}