package thinktank.javabot.physics;

import thinktank.javabot.graphics.DrawableVisitor;

public class Wall extends Placeable{

	protected Wall(BattleField map, Coordinates coord)
	{
		super(map,coord);
	}

	public TypeObjectTT getType(){
		return TypeObjectTT.WALL;
	}
	
	protected void destroy() {
		getBattleField().destroyObjectFromBattlefield(getCoordX(), getCoordY());
		setIsStillExisting(false);
	}
	
	public void accept(DrawableVisitor visitor){
		visitor.visit(this);
	}

}
