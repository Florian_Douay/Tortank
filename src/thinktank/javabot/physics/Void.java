package thinktank.javabot.physics;

import thinktank.javabot.graphics.DrawableVisitor;

public class Void extends Placeable{

	protected Void(BattleField map, Coordinates coord)
	{
		super(map,coord);
	}

	public TypeObjectTT getType(){
		return TypeObjectTT.VOID;
	}
	
	protected void destroy() {
		//destroying objet mean replace it by Void, so void can't be destroyed
	}
	
	public void accept(DrawableVisitor visitor){
		visitor.visit(this);
	}

}
