package thinktank.javabot.physics;

import thinktank.javabot.graphics.DrawableVisitor;

public class Projectile extends Movable{

	private int projectileDamages;

	protected Projectile(int x, int y, Direction direction, BattleField map, int damages)
	{
		super(new Coordinates(x,y),map);
		setDirection(direction);
		setProjectileDamages(damages);
	}
	
	@Override
	protected void forward(){
		if (getCooldownAction() > 0) {
			setCooldownAction(getCooldownAction() - 1);
			return;
		}
		int x = getCoordX();
		int y = getCoordY();
		super.forward();
		if(x != getCoordX() || y!= getCoordY())
			setCooldownAction(getSpeed().getValue());
	}
	

	public int getProjectileDamages(){
		return projectileDamages;
	}

	protected void setProjectileDamages(int damages){
		this.projectileDamages = damages;
	}

	@Override
	public TypeObjectTT getType(){
		return TypeObjectTT.PROJECTILE;
	}
	
	protected void destroy(){
		getBattleField().destroyObjectFromBattlefield(getCoordX(),getCoordY());
		setIsStillExisting(false);
		getBattleField().removeProjectile(this);
	}
	
	public void accept(DrawableVisitor visitor){
		visitor.visit(this);
	}
	
}
