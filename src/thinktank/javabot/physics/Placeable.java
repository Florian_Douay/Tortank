package thinktank.javabot.physics;

import thinktank.javabot.graphics.Drawable;

public abstract class Placeable implements ObjetTT,
										   Drawable{

	private boolean isStillExisting;
	
	protected BattleField map;
	protected Coordinates coord;
	protected Direction direction;
	
	public Placeable(BattleField map, Coordinates coord){
		this.map = map;
		this.coord = coord;
		this.isStillExisting = true;
	}
	
	public boolean isStillExisting(){
		return isStillExisting;
	}
	
	protected void setIsStillExisting(boolean bool) {
		this.isStillExisting = bool;
	}
	
	public BattleField getBattleField(){
		return map;
	}

	protected void setBattleField(BattleField map){
		this.map = map;
	}
	
	public int getCoordX(){
		return coord.getX();
	}

	protected void setCoordX(int x){
		this.coord.setX(x);
	}

	public int getCoordY(){
		return coord.getY();
	}

	protected void setCoordY(int y){
		this.coord.setY(y);
	}

	public Direction getDirection(){
		return direction;
	}

	protected void setDirection(Direction direction){
		this.direction = direction;
	}
	
	protected abstract void destroy();

}
