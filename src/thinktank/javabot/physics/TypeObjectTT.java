package thinktank.javabot.physics;

public enum TypeObjectTT {
	TANK,
	VOID,
	PROJECTILE,
	WALL
}
