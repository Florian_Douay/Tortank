package thinktank.javabot.sensors;

public interface Sensors {

	public ResultsSensor detection(Orientation orientation);
}
