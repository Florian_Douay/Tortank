package thinktank.javabot.sensors;


import thinktank.javabot.physics.Direction;
import thinktank.javabot.physics.Game;
import thinktank.javabot.physics.Tank;
import thinktank.javabot.physics.TypeObjectTT;


public class Radar implements Sensors {
	private Tank objTank;
	private Game game;
	
	public Radar(Tank objTank, Game game) {
		this.objTank = objTank;
		this.game = game;
	}
	
	public ResultsSensor detection(Orientation orientation) {
		
		Direction directionToCheck = new Direction(objTank.getDirection());
		
		switch(orientation){
		case FRONT:
			break;
		case RIGHT:
			directionToCheck.turnOnTheRight();
			break;
		case BEHIND:
			directionToCheck.turnOnTheRight();
			directionToCheck.turnOnTheRight();
			break;
		case LEFT:
			directionToCheck.turnOnTheLeft();
			break;
		//should never happens
		default:
			break;
		}
		
		int x = objTank.getCoordX();
		int y = objTank.getCoordY();
		int dx = directionToCheck.getDx();
		int dy = directionToCheck.getDy();
		
		for(int i=1;i<=10;i++){
			TypeObjectTT contenu = game.getBattlefield().getContentAtLocation(x+i*dx,y+i*dy).getType();
			if(contenu != TypeObjectTT.VOID){
				return  new ResultsSensor(i, contenu);
			}
		}
		return new ResultsSensor(0, TypeObjectTT.VOID);
	}

}
