package thinktank.javabot.sensors;

import thinktank.javabot.physics.TypeObjectTT;

public class ResultsSensor {
	private int distance;
	private TypeObjectTT type;
	
	public ResultsSensor(int distance, TypeObjectTT type) {
		this.distance = distance;
		this.type = type;
	}
	
	public int getDistance() {
		return distance;
	}

	public TypeObjectTT getType() {
		return type;
	}
}
