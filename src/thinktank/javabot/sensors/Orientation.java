package thinktank.javabot.sensors;

public enum Orientation {
	FRONT,
	RIGHT,
	BEHIND,
	LEFT
}
