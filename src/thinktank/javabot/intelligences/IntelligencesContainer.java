package thinktank.javabot.intelligences;

import java.util.ArrayList;


public class IntelligencesContainer {
	
	/** Instance unique pré-initialisée */
	private static IntelligencesContainer s_intelligencesContainerInstance = new IntelligencesContainer();

	/** Constructeur privé pour singleton **/
	private IntelligencesContainer()
	{}

	/** Point d'accès pour l'instance unique du singleton */
	public static IntelligencesContainer getInstance()
	{
		return s_intelligencesContainerInstance;
	}
	
	//----------------------------------------------------------
	
	private int numberOfRunningIntelligences = 0;
	
	private ArrayList<Intelligence> intelligencesArray = new ArrayList<Intelligence>();
	
	
	public Intelligence addIntelligence(Intelligence intel){
		intelligencesArray.add(intel);
		return intel;
	}
	
	public void removeIntelligence(Intelligence intel){
		//todo : try catch
		intelligencesArray.remove(intel);
	}
	
	/**
	 * Renvoi le nombre d'ia qui sont toujours en train de calculer (non endormies).
	 */
	public int getNumberOfRunningIntelligences(){
		return numberOfRunningIntelligences;
	}
	
	/**
	 * Incrémente le compteur (sémaphore) d'intelligences s'éxécutants.
	 */
	synchronized public void incrementNumberOfRunningIntelligences(){
		this.numberOfRunningIntelligences++;
	}
	
	/**
	 * Décrémente le compteur (sémaphore) d'intelligences s'éxécutants.
	 */
	synchronized public void decrementNumberOfRunningIntelligences(){
		this.numberOfRunningIntelligences--;
		if(numberOfRunningIntelligences <= 0)
			this.notifyAll();
	}
	
}
