package thinktank.javabot.intelligences;

import thinktank.javabot.physics.Tank;
import thinktank.javabot.physics.TypeObjectTT;
import thinktank.javabot.sensors.Radar;
import thinktank.javabot.sensors.Orientation;
import thinktank.javabot.sensors.ResultsSensor;


public class TankRemote {
	
	Tank tankObject;
	boolean lock; 	//Booléen pour le vérouillage de la télécommande, pour faire instruction après instruction
	
	TankRemote(Intelligence ia, Tank tankPhy){
		this.tankObject = tankPhy;
		lock = true;
	}
	
	/**
	 *  Vérrouille la télécommande, l'IA associée s'endors. */
	public synchronized void lock(){
		lock = true;
	}
	
	/**
	 *  Dévérrouille la télécommande, l'IA associée est solicité pour un calcul d'action. */
	public synchronized void unlock(){
		lock = false;
		
		/* Wakes up all threads that are waiting on this object's monitor. */
		notifyAll();
	}
	
	public boolean isLocked(){
		return lock;
	}
	
	/**
	 *  Endors l'IA jusqu'à ce que l'instruction soit terminée. */
	public synchronized void lockUntilInstructionFinished(){
		try{
			/* Vérouille la télécommande jusqu'à ce que la commande courante soit exécutée (libéré par unlockForNextInstruction()) */
			this.lock();
			
			IntelligencesContainer.getInstance().decrementNumberOfRunningIntelligences();
			
			/* Attente de signal pour vérifier si la télécommande est débloquée */
			while(isLocked()){
				/* Causes the current thread to wait until another thread invokes the notify() method or the notifyAll() method for this object. */
				wait();
			}
		} 
		catch (InterruptedException e){
			/* Demande d'intéruption du thread détectée */
			Thread.currentThread().interrupt();
		}
	}
	
	/* Ci-dessous les commandes accessibles pour les utilisateurs dans leur script (via la télécommande tank) : */
	
	/**
	 * Do nothing.
	 */
	public void doNothing(){
		tankObject.getAi().setAction(Action.noAction);
		lockUntilInstructionFinished();
	}
	
	/**
	 * Move forward.
	 */
	public void moveForward(){
		tankObject.getAi().setAction(Action.moveForward);
		lockUntilInstructionFinished();
	}
	
	/**
	 * Move backward.
	 */
	public void moveBackward(){
		tankObject.getAi().setAction(Action.moveBackward);
		lockUntilInstructionFinished();
	}
	
	/**
	 * Turn clockwise of tank.
	 */
	public void turnClockwise(){
		tankObject.getAi().setAction(Action.turnClockwise);
		lockUntilInstructionFinished();
	}
	
	/**
	 * Turn counter clockwise of tank.
	 */
	public void turnCounterClockwise(){
		tankObject.getAi().setAction(Action.turnCounterClockwise);
		lockUntilInstructionFinished();
	}
	
	/**
	 * Shoot.
	 */
	public void shoot(){
		tankObject.getAi().setAction(Action.shoot);
		lockUntilInstructionFinished();
	}
	
	/**
	 * Turn clockwise of the tank's canon.
	 */
	public void turnCanonClockwise(){
		tankObject.getAi().setAction(Action.turnCanonClockwise);
		lockUntilInstructionFinished();
	}
	
	/**
	 * Turn counter clockwise of the tank's canon.
	 */
	public void turnCanonCounterClockwise(){
		tankObject.getAi().setAction(Action.turnCanonCounterClockwise);
		lockUntilInstructionFinished();
	}
	
	public ResultsSensor getFrontInformations(){
		Radar dld = ((Radar) tankObject.getSensor());
		return  dld.detection(Orientation.FRONT);
	}
	
	public ResultsSensor getRightInformations(){
		Radar dld = ((Radar) tankObject.getSensor());
		return  dld.detection(Orientation.RIGHT);
	}
	
	public ResultsSensor getBehindInformations(){
		Radar dld = ((Radar) tankObject.getSensor());
		return  dld.detection(Orientation.BEHIND);
	}
	
	public ResultsSensor getLeftInformations(){
		Radar dld = ((Radar) tankObject.getSensor());
		return  dld.detection(Orientation.LEFT);
	}
	
	public TypeObjectTT typeFromString(String str){
		
		return TypeObjectTT.valueOf(str);
	}
	
}