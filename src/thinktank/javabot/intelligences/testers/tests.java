package thinktank.javabot.intelligences.testers;

import thinktank.javabot.intelligences.Intelligence;
import thinktank.javabot.intelligences.IntelligencesContainer;
import thinktank.javabot.physics.Tools;

/**
 * Classe de test pout la partie intelligence artificielle.
 * @author cedric
 *
 */

public class tests {

	/**
	 * Crée de nombreuses IAs et les lances.
	 */
	public static void main(String[] args) {
		
		int nbIntels = 10;
		IntelligencesContainer intels = IntelligencesContainer.getInstance();
		
		Intelligence intel[] = new Intelligence[nbIntels];
		for(int i=0;i<nbIntels;i++){
			Intelligence intelli = new Intelligence(Tools.getScriptsPath() + "tankytourne.py",null);
			intel[i]=intels.addIntelligence(intelli);
			intel[i].startAI();
		}
		
		int i=0;
		int j=0;
		
		while(true){
			
			j++;
			
			for(int k=0;k<nbIntels;k++){
				intel[k].unlockForNextInstruction();
			}
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			for(i=0;i<nbIntels;i++)
				System.out.println("Action d'ia"+i+": "+intel[i].getAction());
			
			System.out.println("Iter n°"+j);
		}
	}	
}