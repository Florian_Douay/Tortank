package thinktank.javabot.intelligences;

// TODO: Auto-generated Javadoc
/**
 * Enumération des actions pouvant être choisies par les scripts IA.
 * @author cedric
 *
 */
public enum Action {
	
	/** The no action. */
	noAction,
	
	/** The move forward. */
	moveForward,
	
	/** The move backward. */
	moveBackward,
	
	/** The turn clockwise of tank. */
	turnClockwise,
	
	/** The turn counter clockwise of tank. */
	turnCounterClockwise,
	
	/** The shoot. */
	shoot,
	
	/** The turn clockwise of the tank's canon. */
	turnCanonClockwise,
	
	/** The turn counter clockwise of the tank's canon. */
	turnCanonCounterClockwise,
	
	/** The script completed. */
	scriptCompleted,
	
	/** The script terminated. */
	scriptTerminated
}
