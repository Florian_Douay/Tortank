package thinktank.javabot.intelligences;
import org.python.util.PythonInterpreter;

import thinktank.javabot.physics.Tank;


public class Intelligence extends Thread {

	/** The tank remote for the user */
	private TankRemote tankR;
	
	/** Filepath to the script */
	private String filepath;
	
	/** Current action asked */
	private Action action;
	
	/* Jython interpreter to read python script file and use functions like it was a java file */
	private PythonInterpreter interp;
	

	public Intelligence(String filepath, Tank tankPhy){
		this.tankR = new TankRemote(this,tankPhy);
		this.filepath = filepath;
		this.action = Action.noAction;
		this.interp = new PythonInterpreter();
		
		/* set default output stream */
		this.interp.setOut(System.out);
		
		/* declare tank variable for python user namespace (it's the user's remote to allow him to control his tank) */
		this.interp.set("tank", tankR);
	}
	
	public String getFilePath(){
		return this.filepath;
	}
	
	public synchronized void startAI(){
		/* start the thread, meaning launching the run() function by the thread */
		this.start();
	}
	
	/**
	 *  
	 * Retourne l'action calculée par le script avec computeAction. Il faut penser
	 * à laiser le temps au script de calculer (delay + timeout).
	 *
	 */
	public Action getAction(){
		return action;
	}
	
	/**
	 * Fonction interne au package. Permet au script python, via un objet TankRemote, de stocker l'action à effectuer.
	 *
	 */
	synchronized void setAction(Action action){
		this.action = action;
		notifyAll();
	}
	
	/**
	 *  Reprend le script python où il était bloqué pour exécuter la prochaine action.
	 */
	public void unlockForNextInstruction(){
		if(action == Action.scriptCompleted || action == Action.scriptTerminated)
			return;

		/* L'action a été effectuée. On écrase l'instruction pour ne pas la refaire */
		this.action = Action.noAction;
		
		IntelligencesContainer.getInstance().incrementNumberOfRunningIntelligences();

		/* Libère la télécommande pour exécuter la prochaine instruction (était vérouillée par lockUntilInstructionFinished()) */
		tankR.unlock();
	}
	
	/**
	 *  Thread dédié à l'interpréteur python pour l'IA utilisateur. */
	public void run(){
		
		IntelligencesContainer.getInstance().incrementNumberOfRunningIntelligences();
	
		/* launch the python script, supposed to be infinite until the tank die */
		interp.execfile(filepath);
		
		/* Allows the running thread to know that the script is finished */
		setAction(Action.scriptCompleted);
		
		IntelligencesContainer.getInstance().decrementNumberOfRunningIntelligences();
	}
}